var allManagers;
function getAllManagers() {
	$
			.getJSON(
					"http://localhost:8080/TravelExpensePlanner/rest/managers",
					function(managers) {
						allManagers = managers;
						var tableContent = "";
						for (var i = 0; i < managers.length; i++) {
							tableContent = tableContent
									+ "<tr><td>"
									+ managers[i].firstName
									+ "</td><td>"
									+ managers[i].lastName
									+ "</td><td>"
									+ managers[i].phone
									+ "</td><td>"
									+ managers[i].email
									+ "</td><td>"
									+ managers[i].socialSecurity
									+ "</td><td>"
									+ managers[i].profession
									+ "</td><td>"
									+ managers[i].department
									+ "</td><td>"
									+ '<button type="button" class="btn btn-lg btn-primary" data-toggle="modal" data-target="#myModal" data-whatever="'
									+ i
									+ '">Edit</button>'
									+ "</td><td>"
									+ '<button type ="button" class = "btn btn-danger" onClick="deleteManager('
									+ managers[i].id + ')">Delete</button>'
									+ "</td></tr>";
						}
						document.getElementById("managerTableBody").innerHTML = tableContent;
					});
}
getAllManagers();

function updateManager() {
	input_box = confirm("Are you sure you want to update this info?");
	if (input_box == true) {

		var updateManagerData = JSON.stringify($("#userForm")
				.serializeFormJSON());
		$
				.ajax({
					url : "http://localhost:8080/TravelExpensePlanner/rest/managers",
					cache : false,
					type : 'PUT',
					data : updateManagerData,
					success : function(updateManagerData) {
						location.reload();
					},
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						console.log("Status: " + textStatus);
						console.log("Error: " + errorThrown);
					},
					contentType : "application/json; charset=utf-8"
				});
	} else {
		return false;
	}
};

function deleteManager(managersID) {
	input_box = confirm("Are you sure you want to delete this Manager?");
	if (input_box == true) {
		$
				.ajax({
					url : "http://localhost:8080/TravelExpensePlanner/rest/managers/"
							+ managersID,
					cache : false,
					type : 'DELETE',
					success : function(DeleteManagerData) {
						location.reload();
					},
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						console.log("Status: " + textStatus);
						console.log("Error: " + errorThrown);
					},
					contentType : "application/json; charset=utf-8"
				});
	} else {
		return false;
	}
};


$('form').submit(function(event) {
	event.preventDefault();
	var newManagerData = JSON.stringify($(this).serializeFormJSON());
	console.log(newManagerData);

	$.ajax({
		url : "http://localhost:8080/TravelExpensePlanner/rest/managers",
		cache : false,
		type : 'POST',
		data : newManagerData,
		success : function(newManagerData) {
			location.reload();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Status: " + textStatus);
			console.log("Error: " + errorThrown);
		},
		contentType : "application/json; charset=utf-8"

	});
});

(function($) {
	$.fn.serializeFormJSON = function() {

		var o = {};
		var a = this.serializeArray();
		$.each(a, function() {
			if (o[this.name]) {
				if (!o[this.name].push) {
					o[this.name] = [ o[this.name] ];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};
})(jQuery);

$('#myModal')
.on(
		'show.bs.modal',
		function(event) {
			var button = $(event.relatedTarget)
			var recipient = button.data('whatever')
			var oneManager;
			function getManager(managersID) {
				$
						.getJSON(
								"http://localhost:8080/TravelExpensePlanner/rest/managers/"
										+ managersID,
								function(managers) {
									oneManager = managers;
									var tableContent2 = "";
									{
										tableContent2 = tableContent
												+ "<tr><td>"
												+ managers.id
												+ "</td><td>"
												+ managers.firstName
												+ "</td><td>"
												+ managers.lastName
												+ "</td><td>"
												+ managers.phone
												+ "</td><td>"
												+ managers.email
												+ "</td><td>"
												+ managers.socialSecurity
												+ "</td><td>"
												+ managers.profession
												+ "</td><td>"
												+ managers.department
												+ "</td></tr>";
									}
									document
											.getElementById("managerTableBody").innerHTML = tableContent;
								});
			}
			var modal = $(this)
			modal.find('.modal-body input#id').val(
					allManagers[recipient].id)
			modal.find('.modal-body input#firstName').val(
					allManagers[recipient].firstName)
			modal.find('.modal-body input#lastName').val(
					allManagers[recipient].lastName)
			modal.find('.modal-body input#phone').val(
					allManagers[recipient].phone)
			modal.find('.modal-body input#email').val(
					allManagers[recipient].email)
			modal.find('.modal-body input#socialSecurity').val(
					allManagers[recipient].socialSecurity)
			modal.find('.modal-body input#profession').val(
					allManagers[recipient].profession)
			modal.find('.modal-body input#department').val(
					allManagers[recipient].department)
		})