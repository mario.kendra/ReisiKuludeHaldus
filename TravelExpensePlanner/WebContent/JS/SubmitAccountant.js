var allAccountants;
function getAllAccountants() {
	$
			.getJSON(
					"http://localhost:8080/TravelExpensePlanner/rest/accountants",
					function(accountants) {
						allAccountants = accountants;
						var tableContent = "";
						for (var i = 0; i < accountants.length; i++) {
							tableContent = tableContent + "<tr><td>"
									+ accountants[i].firstName + "</td><td>"
									+ accountants[i].lastName + "</td><td>"
									+ accountants[i].phone + "</td><td>"
									+ accountants[i].email + "</td><td>"
									+ accountants[i].socialSecurity
									+ "</td><td>" + accountants[i].profession
									+ "</td><td>"
									+ '<button type="button" class="btn btn-lg btn-primary" data-toggle="modal" data-target="#myModal" data-whatever="'
									+ i
									+ '">Edit</button>'
									+ "</td><td>"
									+ '<button type ="button" class = "btn btn-danger" onClick="deleteAccountant('
									+ accountants[i].id + ')">Delete</button>'
									+ "</td></tr>";
						}
						document.getElementById("accountantTableBody").innerHTML = tableContent;
					});
}
getAllAccountants();

function updateAccountant() {
	input_box = confirm("Are you sure you want to update this info?");
	if (input_box == true) {

		var updateAccountantData = JSON.stringify($("#userForm")
				.serializeFormJSON());
		$
				.ajax({
					url : "http://localhost:8080/TravelExpensePlanner/rest/accountants",
					cache : false,
					type : 'PUT',
					data : updateAccountantData,
					success : function(updateAccountantData) {
						location.reload();
					},
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						console.log("Status: " + textStatus);
						console.log("Error: " + errorThrown);
					},
					contentType : "application/json; charset=utf-8"
				});
	} else {
		return false;
	}
};

function deleteAccountant(accountantsID) {
	input_box = confirm("Are you sure you want to delete this Accountant?");
	if (input_box == true) {
		$
				.ajax({
					url : "http://localhost:8080/TravelExpensePlanner/rest/accountants/"
							+ accountantsID,
					cache : false,
					type : 'DELETE',
					success : function(DeleteAccountantData) {
						location.reload();
					},
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						console.log("Status: " + textStatus);
						console.log("Error: " + errorThrown);
					},
					contentType : "application/json; charset=utf-8"
				});
	} else {
		return false;
	}
};

$('form').submit(function(event) {
	event.preventDefault();
	var newAccountantData = JSON.stringify($(this).serializeFormJSON());
	console.log(newAccountantData);

	$.ajax({
		url : "http://localhost:8080/TravelExpensePlanner/rest/accountants",
		cache : false,
		type : 'POST',
		data : newAccountantData,
		success : function(newAccountantData) {
			location.reload();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Status: " + textStatus);
			console.log("Error: " + errorThrown);
		},
		contentType : "application/json; charset=utf-8"

	});
});

(function($) {
	$.fn.serializeFormJSON = function() {

		var o = {};
		var a = this.serializeArray();
		$.each(a, function() {
			if (o[this.name]) {
				if (!o[this.name].push) {
					o[this.name] = [ o[this.name] ];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};
})(jQuery);

$('#myModal')
.on(
		'show.bs.modal',
		function(event) {
			var button = $(event.relatedTarget)
			var recipient = button.data('whatever')
			var oneAccountant;
			function getAccountant(accountantsID) {
				$
						.getJSON(
								"http://localhost:8080/TravelExpensePlanner/rest/accountants/"
										+ accountantsID,
								function(accountants) {
									oneAccountant = accountants;
									var tableContent2 = "";
									{
										tableContent2 = tableContent
												+ "<tr><td>"
												+ accountants.id
												+ "</td><td>"
												+ accountants.firstName
												+ "</td><td>"
												+ accountants.lastName
												+ "</td><td>"
												+ accountants.phone
												+ "</td><td>"
												+ accountants.email
												+ "</td><td>"
												+ accountants.socialSecurity
												+ "</td><td>"
												+ accountants.profession
												+ "</td></tr>";
									}
									document
											.getElementById("accountantTableBody").innerHTML = tableContent;
								});
			}
			var modal = $(this)
			modal.find('.modal-body input#id').val(
					allAccountants[recipient].id)
			modal.find('.modal-body input#firstName').val(
					allAccountants[recipient].firstName)
			modal.find('.modal-body input#lastName').val(
					allAccountants[recipient].lastName)
			modal.find('.modal-body input#phone').val(
					allAccountants[recipient].phone)
			modal.find('.modal-body input#email').val(
					allAccountants[recipient].email)
			modal.find('.modal-body input#socialSecurity').val(
					allAccountants[recipient].socialSecurity)
			modal.find('.modal-body input#profession').val(
					allAccountants[recipient].profession)
		})