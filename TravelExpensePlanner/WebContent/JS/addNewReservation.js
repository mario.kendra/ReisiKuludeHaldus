$('form').submit(function(event) {
	event.preventDefault();
	var newReservationData = JSON.stringify($(this).serializeFormJSON());
	console.log(newReservationData);

	$.ajax({
		url : "http://localhost:8080/TravelExpensePlanner/rest/reservations",
		cache : false,
		type : 'POST',
		data : newReservationData,
		success : function(newReservationData) {
			location.reload();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Status: " + textStatus);
			console.log("Error: " + errorThrown);
		},
		contentType : "application/json; charset=utf-8"

	});
});

(function($) {
	$.fn.serializeFormJSON = function() {

		var o = {};
		var a = this.serializeArray();
		$.each(a, function() {
			if (o[this.name]) {
				if (!o[this.name].push) {
					o[this.name] = [ o[this.name] ];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};
})(jQuery);
