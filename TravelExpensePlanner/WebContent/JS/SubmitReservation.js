var allReservations;
function getAllReservations() {
	$
			.getJSON(
					"http://localhost:8080/TravelExpensePlanner/rest/reservations",
					function(reservations) {
						allReservations = reservations;
						var tableContent = "";
						for (var i = 0; i < reservations.length; i++) {
							tableContent = tableContent
									+ "<tr><td>"
									+ reservations[i].firstName
									+ "</td><td>"
									+ reservations[i].lastName
									+ "</td><td>"
									+ reservations[i].departure
									+ "</td><td>"
									+ reservations[i].destination
									+ "</td><td>"
									+ reservations[i].startDate
									+ "</td><td>"
									+ reservations[i].endDate
									+ "</td><td>"
									+ reservations[i].tripDescription
									+ "</td><td>"
									+ reservations[i].estimatedCost + "€"
									+ "</td><td>"
									+ '<button type="button" class="btn btn-lg btn-success" data-toggle="modal" data-target="#myModal2" data-report="'
									+ i
									+ '">Add new Report</button>'
									+ "</td><td>"
									+ '<button type="button" class="btn btn-lg btn-primary" data-toggle="modal" data-target="#myModal" data-whatever="'
									+ i
									+ '">Edit</button>'
									+ "</td><td>"
									+ '<button type = "button" class = "btn btn-danger" onClick="deleteReservation('
									+ reservations[i].id + ')">Delete</button>'
									+ "</td></tr>";
						}
						document.getElementById("reservationsTableBody").innerHTML = tableContent;

					});

}

getAllReservations();

function updateReservation() {
	input_box = confirm("Are you sure you want to update this Reservation?");
	if (input_box == true) {

		var updateReservationData = JSON.stringify($("#userForm")
				.serializeFormJSON());
		$
				.ajax({
					url : "http://localhost:8080/TravelExpensePlanner/rest/reservations",
					cache : false,
					type : 'PUT',
					data : updateReservationData,
					success : function(updateReservationData) {
						location.reload();
					},
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						console.log("Status: " + textStatus);
						console.log("Error: " + errorThrown);
					},
					contentType : "application/json; charset=utf-8"

				});

	} else {
		return false;
	}
};

function deleteReservation(reservationsID) {
	input_box = confirm("Are you sure you want to delete this Reservation?");
	if (input_box == true) {
		$

				.ajax({
					url : "http://localhost:8080/TravelExpensePlanner/rest/reservations/"
							+ reservationsID,
					cache : false,
					type : 'DELETE',
					success : function(DeleteReservationData) {
						location.reload();
					},
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						console.log("Status: " + textStatus);
						console.log("Error: " + errorThrown);
					},
					contentType : "application/json; charset=utf-8"
				});
	} else {
		return false;
	}
};

$('form').submit(function(event) {
	event.preventDefault();
	var newReservationData = JSON.stringify($(this).serializeFormJSON());
	console.log(newReservationData);

	$.ajax({
		url : "http://localhost:8080/TravelExpensePlanner/rest/reservations",
		cache : false,
		type : 'POST',
		data : newReservationData,
		success : function(newReservationData) {
			location.reload();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Status: " + textStatus);
			console.log("Error: " + errorThrown);
		},
		contentType : "application/json; charset=utf-8"

	});
});

(function($) {
	$.fn.serializeFormJSON = function() {

		var o = {};
		var a = this.serializeArray();
		$.each(a, function() {
			if (o[this.name]) {
				if (!o[this.name].push) {
					o[this.name] = [ o[this.name] ];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};
})(jQuery);

$('#myModal')
		.on(
				'show.bs.modal',
				function(event) {
					var button = $(event.relatedTarget) // Button that triggered
					// the modal
					var recipient = button.data('whatever') // Extract info from
					// data-* attributes
					// If necessary, you could initiate an AJAX request here
					// (and then do the updating in a callback).
					var oneReservation;
					function getReservation(reservationsID) {
						$
								.getJSON(
										"http://localhost:8080/TravelExpensePlanner/rest/reservations/"
												+ reservationsID,
										function(reservations) {
											oneReservation = reservations;
											var tableContent2 = "";
											{
												tableContent2 = tableContent
														+ "<tr><td>"
														+ reservations.id
														+ "</td><td>"
														+ reservations.firstName
														+ "</td><td>"
														+ reservations.lastName
														+ "</td><td>"
														+ reservations.departure
														+ "</td><td>"
														+ reservations.destination
														+ "</td><td>"
														+ reservations.startDate
														+ "</td><td>"
														+ reservations.endDate
														+ "</td><td>"
														+ reservations.transportationType
														+ "</td><td>"
														+ reservations.transportationCost
														+ "€"
														+ "</td><td>"
														+ reservations.accommodation
														+ "</td><td>"
														+ reservations.accommodationCost
														+ "€"
														+ "</td><td>"
														+ reservations.tripDescription
														+ "</td></tr>";
											}
											document
													.getElementById("reservationTableBody").innerHTML = tableContent;

										});
					}
					// getReservation(reservationsID);

					// Update the modal's content. We'll use jQuery here, but
					// you could use a data binding library or other methods
					// instead.
					var modal = $(this)
					// modal.find('.modal-title').text('New message to ' +
					// recipient)
					modal.find('.modal-body input#id').val(
							allReservations[recipient].id)
					modal.find('.modal-body input#firstName').val(
							allReservations[recipient].firstName)
					modal.find('.modal-body input#lastName').val(
							allReservations[recipient].lastName)
					modal.find('.modal-body input#departure').val(
							allReservations[recipient].departure)
					modal.find('.modal-body input#destination').val(
							allReservations[recipient].destination)
					modal.find('.modal-body input#startDate').val(
							allReservations[recipient].startDate)
					modal.find('.modal-body input#endDate').val(
							allReservations[recipient].endDate)
					modal.find('.modal-body input#transportationType').val(
							allReservations[recipient].transportationType)
					modal.find('.modal-body input#transportationCost').val(
							allReservations[recipient].transportationCost)
					modal.find('.modal-body input#accommodation').val(
							allReservations[recipient].accommodation)
					modal.find('.modal-body input#accommodationCost').val(
							allReservations[recipient].accommodationCost)
					modal.find('.modal-body input#tripDescription').val(
							allReservations[recipient].tripDescription)
				})

$('#myModal2')
		.on(
				'show.bs.modal',
				function(event) {
					var button = $(event.relatedTarget) // Button that triggered
					// the modal
					var recipient2 = button.data('report') // Extract info from
					// data-* attributes
					// If necessary, you could initiate an AJAX request here
					// (and then do the updating in a callback).
					var oneReservation2;
					function getReservation(reservationsID) {
						$
								.getJSON(
										"http://localhost:8080/TravelExpensePlanner/rest/reservations/"
												+ reservationsID,
										function(reservations) {
											oneReservation2 = reservations;
											var tableContent3 = "";
											{
												tableContent3 = tableContent
														+ "<tr><td>"
														+ reservations.id
														+ "</td><td>"
														+ reservations.firstName
														+ "</td><td>"
														+ reservations.lastName
														+ "</td><td>"
														+ reservations.departure
														+ "</td><td>"
														+ reservations.destination
														+ "</td><td>"
														+ reservations.startDate
														+ "</td><td>"
														+ reservations.endDate
														+ "</td><td>"
														+ reservations.transportationType
														+ "</td><td>"
														+ reservations.transportationCost
														+ "€"
														+ "</td><td>"
														+ reservations.accommodation
														+ "</td><td>"
														+ reservations.accommodationCost
														+ "€"
														+ "</td><td>"
														+ reservations.tripDescription
														+ "</td></tr>";
											}
											document
													.getElementById("reservationTableBody").innerHTML = tableContent;

										});
					}
					// getReservation(reservationsID);

					// Update the modal's content. We'll use jQuery here, but
					// you could use a data binding library or other methods
					// instead.
					var modal2 = $(this)
					console.log(allReservations[recipient2])
					// modal.find('.modal-title').text('New message to ' +
					// recipient)
					modal2.find('.modal-body input#id').val(
							allReservations[recipient2].id)
					modal2.find('.modal-body input#firstName2').val(
							allReservations[recipient2].firstName)
					modal2.find('.modal-body input#lastName2').val(
							allReservations[recipient2].lastName)
					modal2.find('.modal-body input#departure2').val(
							allReservations[recipient2].departure)
					modal2.find('.modal-body input#destination2').val(
							allReservations[recipient2].destination)
					modal2.find('.modal-body input#startDate2').val(
							allReservations[recipient2].startDate)
					modal2.find('.modal-body input#endDate2').val(
							allReservations[recipient2].endDate)
					modal2.find('.modal-body input#transportationType2').val(
							allReservations[recipient2].transportationType)
					modal2.find('.modal-body input#transportationCost2').val(
							allReservations[recipient2].transportationCost)
					modal2.find('.modal-body input#accommodation2').val(
							allReservations[recipient2].accommodation)
					modal2.find('.modal-body input#accommodationCost2').val(
							allReservations[recipient2].accommodationCost)
					modal2.find('.modal-body input#tripDescription2').val(
							allReservations[recipient2].tripDescription)
				})