<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="CSS/login.css">
<title>Travel Expense Planner</title>
</head>
<body>
   <div class="login">
    <h1>Login</h1>
    <form action="loginServlet" method="post">
    	<input type="text" name="username" placeholder="username" required="required" />
        <input type="password" name="userpass" placeholder="password" required="required" />
        <button type="submit" class="btn btn-primary btn-block btn-large" value="Login">Log in</button>
    </form>
    <br>
</div>
</body>
</html>
