package com.mkendra.travelexpense.bean;

public class Accountant extends Person {
	
	public Accountant(){
		super();
	}

	public Accountant(int id, String firstName, String lastName, String phone,
			String email, String socialSecurity, int profession) {
		super(id, firstName, lastName, phone, email, socialSecurity, profession);
	}
}