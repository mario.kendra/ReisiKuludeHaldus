package com.mkendra.travelexpense.bean;

import java.math.BigDecimal;

public class ExtraCost {

	int extraCostId;
	String extraCostType;
	BigDecimal extraCostPrice;
	String extraCostDate;
	int reservationId;

	public ExtraCost() {
	}

	public ExtraCost(int extraCostId, String extraCostType, BigDecimal extraCostPrice, String extraCostDate, int reservationId) {
		this.extraCostId = extraCostId;
		this.extraCostType = extraCostType;
		this.extraCostDate = extraCostDate;
		this.extraCostPrice = extraCostPrice;
		this.reservationId = reservationId;

	}

	public int getExtraCostId() {
		return extraCostId;
	}

	public void setExtraCostId(int extraCostId) {
		this.extraCostId = extraCostId;
	}

	public String getExtraCostType() {
		return extraCostType;
	}

	public void setExtraCostType(String extraCostType) {
		this.extraCostType = extraCostType;
	}

	public int getReservationId() {
		return reservationId;
	}

	public void setReservationId(int reservationId) {
		this.reservationId = reservationId;
	}

	public BigDecimal getExtraCostPrice() {
		return extraCostPrice;
	}

	public void setExtraCostPrice(BigDecimal extraCostPrice) {
		this.extraCostPrice = extraCostPrice;
	}

	public String getExtraCostDate() {
		return extraCostDate;
	}

	public void setExtraCostDate(String extraCostString) {
		this.extraCostDate = extraCostString;
	}

}
