package com.mkendra.travelexpense.bean;

public class Reservation {

	int id;
	String firstName;
	String lastName;
	String departure;
	String destination;
	String startDate;
	String endDate;
	String transportationType;
	int transportationCost;
	String accommodation;
	int accommodationCost;
	String tripDescription;
	int estimatedCost;

	public Reservation() {
	}

	public Reservation(int i, String firstName, String lastName,
			String departure, String destination, String startDate,
			String endDate, String transportationType, int transportationCost,
			String accommodation, int accommodationCost,
			String tripDescription, int estimatedCost) {

		this.id = i;
		this.firstName = firstName;
		this.lastName = lastName;
		this.departure = departure;
		this.destination = destination;
		this.startDate = startDate;
		this.endDate = endDate;
		this.transportationType = transportationType;
		this.transportationCost = transportationCost;
		this.accommodation = accommodation;
		this.accommodationCost = accommodationCost;
		this.tripDescription = tripDescription;
		this.estimatedCost = transportationCost + accommodationCost;
	}
		
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDeparture() {
		return departure;
	}

	public void setDeparture(String departure) {
		this.departure = departure;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getTransportationType() {
		return transportationType;
	}

	public void setTransportationType(String transportationType) {
		this.transportationType = transportationType;
	}

	public int getTransportationCost() {
		return transportationCost;
	}

	public void setTransportationCost(int transportationCost) {
		this.transportationCost = transportationCost;
	}

	public String getAccommodation() {
		return accommodation;
	}

	public void setAccommodation(String accommodation) {
		this.accommodation = accommodation;
	}

	public int getAccommodationCost() {
		return accommodationCost;
	}

	public void setAccommodationCost(int accommodationCost) {
		this.accommodationCost = accommodationCost;
	}

	public String getTripDescription() {
		return tripDescription;
	}

	public void setTripDescription(String tripDescription) {
		this.tripDescription = tripDescription;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEstimatedCost() {
		return estimatedCost;
	}

	public void setEstimatedCost(int estimatedCost) {
		this.estimatedCost = estimatedCost;
	}
}
