package com.mkendra.travelexpense.controller;

import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.mkendra.travelexpense.bean.Manager;
import com.mkendra.travelexpense.service.ManagerService;;

@Path("/managers")
public class ManagerController {
	ManagerService managerService = new ManagerService();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Manager> getAllManagers() {
		List<Manager> listOfManagers = managerService.getAllManagers();
		return listOfManagers;
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Manager getManagerById(@PathParam("id") int id) {
		return managerService.getManager(id);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Manager addManager(Manager manager) {
		return managerService.addManager(manager);
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Manager updateManager(Manager manager) {
		return managerService.updateManager(manager);
	}

	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public void deleteManager(@PathParam("id") int id) {
		managerService.deleteManager(id);
	}

}
