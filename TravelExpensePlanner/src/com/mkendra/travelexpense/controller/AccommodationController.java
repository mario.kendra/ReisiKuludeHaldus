package com.mkendra.travelexpense.controller;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.mkendra.travelexpense.bean.Accommodation;
import com.mkendra.travelexpense.service.AccommodationService;;

@Path("/accommodation")
public class AccommodationController {	
		 AccommodationService accommodationService=new AccommodationService();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Accommodation> getAccommodations() {

		List<Accommodation> listOfAccommodations = accommodationService.getAllAccommodations();
		return listOfAccommodations;
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Accommodation getAccommodationById(@PathParam("id") int id) {
		return accommodationService.getAccommodation(id);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Accommodation addAccommodation(Accommodation accommodation) {
		return accommodationService.addAccommodation(accommodation);
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Accommodation updateAccommodation(Accommodation accommodation) {
		return accommodationService.updateAccommodation(accommodation);

	}

	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public void deleteAccommodation(@PathParam("id") int id) {
		accommodationService.deleteAccommodation(id);

	}

}
