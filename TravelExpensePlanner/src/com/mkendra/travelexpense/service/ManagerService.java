package com.mkendra.travelexpense.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.mkendra.travelexpense.bean.Manager;

public class ManagerService {

	public ManagerService() {
		super();
	}
	
	public List<Manager> getAllManagers() {
		List<Manager> Managers = new ArrayList<Manager>();
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(
						"Select * from reisikuludeplaneerimine.managers;")) {

			while (resultSet.next()) {

				Managers.add(new Manager(resultSet.getInt("managersID"),
						resultSet.getString("first_name"),
						resultSet.getString("last_name"),
						resultSet.getString("phone"),
						resultSet.getString("email"),
						resultSet.getString("social_security"),
						resultSet.getInt("occupationID"),
						resultSet.getString("department")));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return Managers;
	}
	
	public Manager getManager(int id) {
		Manager manager = new Manager();
		Connection connection = DBConnectionHandling.createConnection();

		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(
						"Select * from reisikuludeplaneerimine.managers where managersID = "
								+ id + ";");) {

			while (resultSet.next()) {
				manager.setFirstName(resultSet.getString("first_name"));
				;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return manager;
	}
	
	public Manager addManager(Manager manager) {
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();) {
			statement.executeUpdate(
					"Insert into reisikuludeplaneerimine.managers(first_name, last_name, phone, email, social_security, occupationID, department) VALUES ('"
							+ manager.getFirstName() + "', '"
							+ manager.getLastName() + "','"
							+ manager.getPhone() + "','"
							+ manager.getEmail() + "','"
							+ manager.getSocialSecurity() + "', "
							+ manager.getProfession() + ", '"
							+ manager.getDepartment() + "');");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		DBConnectionHandling.closeConnection(connection);
		return manager;
	}
	
	public Manager deleteManager(int id) {
		Manager manager = new Manager();
		Connection connection = DBConnectionHandling.createConnection();

		String deleteTableSQL = "DELETE FROM ReisiKuludePlaneerimine.managers WHERE managersID = "
				+ id + "";

		try (Statement statement = connection.createStatement();) {
			statement.execute(deleteTableSQL);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return manager;
	}

	public Manager updateManager(Manager manager) {
		Connection connection = DBConnectionHandling.createConnection();

		String updateTableSQL = "UPDATE reisikuludeplaneerimine.managers SET first_name='"
				+ manager.getFirstName() + "', last_name= '"
				+ manager.getLastName() + "', phone= '" + manager.getPhone()
				+ "', email='" + manager.getEmail() + "', social_security='"
				+ manager.getSocialSecurity() + "', occupationID="
				+ manager.getProfession() + ", department='"
				+ manager.getDepartment() + "' WHERE managersID ="
				+ manager.getId() + ";";

		try (Statement statement = connection.createStatement();) {
			statement.executeUpdate(updateTableSQL);
		} catch (

		SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return manager;
	}
}
