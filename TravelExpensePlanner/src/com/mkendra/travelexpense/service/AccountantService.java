package com.mkendra.travelexpense.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mkendra.travelexpense.bean.Accountant;

public class AccountantService {

	public AccountantService() {
		super();
	}

	public List<Accountant> getAllAccountants() {
		List<Accountant> accountants = new ArrayList<Accountant>();
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(
						"Select * from reisikuludeplaneerimine.accountants;")) {

			while (resultSet.next()) {

				accountants
						.add(new Accountant(resultSet.getInt("accountantsID"),
								resultSet.getString("first_name"),
								resultSet.getString("last_name"),
								resultSet.getString("phone"),
								resultSet.getString("email"),
								resultSet.getString("social_security"),
								resultSet.getInt("occupationID")));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return accountants;

	}

	public Accountant getAccountant(int id) {
		Accountant accountant = new Accountant();
		Connection connection = DBConnectionHandling.createConnection();

		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(
						"Select * from reisikuludeplaneerimine.accountants where accountantsID = "
								+ id + ";");) {

			while (resultSet.next()) {
				accountant.setFirstName(resultSet.getString("first_name"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return accountant;
	}

	public Accountant addAccountant(Accountant accountant) {
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();) {
			statement.executeUpdate(
					"Insert into reisikuludeplaneerimine.accountants(first_name, last_name, phone, email, social_security, occupationID) VALUES ('"
							+ accountant.getFirstName() + "', '"
							+ accountant.getLastName() + "','"
							+ accountant.getPhone() + "','"
							+ accountant.getEmail() + "','"
							+ accountant.getSocialSecurity() + "', "
							+ accountant.getProfession() + ");");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		DBConnectionHandling.closeConnection(connection);
		return accountant;
	}

	public Accountant deleteAccountant(int id) {
		Accountant accountant = new Accountant();
		Connection connection = DBConnectionHandling.createConnection();

		String deleteTableSQL = "DELETE FROM ReisiKuludePlaneerimine.accountants WHERE accountantsID = "
				+ id + "";

		try (Statement statement = connection.createStatement();) {
			statement.execute(deleteTableSQL);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return accountant;
	}

	public Accountant updateAccountant(Accountant accountant) {
		Connection connection = DBConnectionHandling.createConnection();

		String updateTableSQL = "UPDATE reisikuludeplaneerimine.accountants SET first_name='"
				+ accountant.getFirstName() + "', last_name= '"
				+ accountant.getLastName() + "', phone= '"
				+ accountant.getPhone() + "', email='" + accountant.getEmail()
				+ "', social_security='" + accountant.getSocialSecurity()
				+ "', occupationID=" + accountant.getProfession()
				+ " WHERE accountantsID =" + accountant.getId() + ";";

		try (Statement statement = connection.createStatement();) {
			statement.executeUpdate(updateTableSQL);
		} catch (

		SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return accountant;
	}
}